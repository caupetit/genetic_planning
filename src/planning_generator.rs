use worker::Worker;
use worker_analysis::WorkerAnalysis;
use worker_analysis::cubic;
use genetics::genetic::Dna;

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum Day {
    Monday,
    Tuesday,
    Wednesday,
    Thursday,
    Friday,
    Saturday,
    Sunday,
}

pub struct WorkPeriod {
    pub week: i32,
    pub day: Day,
    pub start: i32,
    pub slots: Vec<Option<i32>>,
    pub expected: i32,
    pub filled: i32,
}

pub struct Period {
    pub start: i32,
    pub len: i32,
}

pub struct Expectation {
    pub period: Period,
    pub expected: i32,
}

impl Expectation {
    pub fn new (start: i32, len: i32, expected: i32) -> Expectation {
        Expectation { period: Period { start, len }, expected }
    }
}

pub struct PlanningGenerator {
    periods: Vec<WorkPeriod>,
    pub workers: Vec<WorkerAnalysis>,
}

impl PlanningGenerator {
    pub fn new(workers: &Vec<Worker>, expectations: &Vec<(Day, Vec<Expectation>)>) -> PlanningGenerator {
        let work_periods = work_period_from_expectations(workers.len(), expectations);

        let mut worker_analyses = Vec::new();
        for worker in workers {
            worker_analyses.push(WorkerAnalysis::new(worker.id as i32, worker.due_hours));
        }

        PlanningGenerator {
            periods: work_periods,
            workers: worker_analyses,
        }
    }

    pub fn reset(&mut self) {
        self.workers.iter_mut().for_each(|w| w.reset());
        self.periods.iter_mut().for_each(|p| p.filled = 0);
    }

    fn filled_hours_impact(&self, total_impact: f32) -> i32 {
        let mut score = 0;
        let num_workers =  self.workers.len() as i32;
        let num_periods = self.periods.len() as i32;
        let total_expected: i32 = self.periods.iter().map(|w| w.expected).sum();
        let total_possible_hours = num_periods * num_workers;

        let mut total_over = 0;
        let mut total_missing = 0;
        for period in self.periods.iter() {
            let over_hours = period.expected - period.filled;
            if over_hours < 0 {
                total_over += over_hours.abs();
            } else if over_hours > 0 {
                total_missing += over_hours
            }
        }

        let max_over_hours = (total_possible_hours - total_expected) as f32;
        let max_missing_hours = total_expected as f32;
        score += (total_impact * 0.6 * (1.0 - total_missing as f32 / max_missing_hours)) as i32;
        score += (total_impact * 0.4 * (1.0 - total_over as f32/ max_over_hours)) as i32;

        up_if_max(score, total_impact as i32)
    }

    fn work_period_len_impact(&self, total_impact: f32) -> i32 {
        let num_workers =  self.workers.len() as i32;
        let work_periods = self.workers.iter().map(|w| {
            w.work_periods.iter().flatten()
        }).flatten();
        let mut total_less = 0;
        let mut total_more = 0;
        for period in work_periods {
            let period = *period;
            if period < 4 {
                total_less += 4 - period
            } else if period > 10 {
                total_more += period - 10
            }
        }

        let max_less = (18 / 3 * 3 * 7 * num_workers) as f32; // should be 18 / 2 but it never happens...
        let max_more = ((17 % 10) * 7 * num_workers) as f32;
        let mut score = 0;
        score += (total_impact * 0.4 * (1.0 - (total_less as f32 / max_less))) as i32;
        score += (total_impact * 0.6 * (1.0 - (total_more as f32 / max_more))) as i32;
        up_if_max(score, total_impact as i32)
    }

    fn off_hours_impact(&self, total_impact: f32) -> i32 {
        let max_off_hours_impact = 17.0 / 2.0 * 4.5 * 7.0 * self.workers.len() as f32;
        let max_off_hours_impact = (max_off_hours_impact as i32) as  f32;

        let off_periods = self.workers.iter().map(|w| {
            let l = w.off_periods.len();
            w.off_periods[..l - 1].iter()
        }).flatten();

        let mut bad_off_hours = 0;
        for i in off_periods {
            let i = *i;
            if i > 3 && i < 12 {
                bad_off_hours += i - 3;
            }
        }

        up_if_max((total_impact * (1.0 - (bad_off_hours as f32 / max_off_hours_impact))) as i32, total_impact as i32)
    }

    fn day_hours_impact(&self, total_impact: f32) -> i32 {
        let max_over_hours = (7 * 7 * self.workers.len()) as f32;
        let over_hours = self.workers.iter().map(|w| {
            let l = w.work_periods.len();
            w.work_periods[..l - 1].iter().map(|l| l.iter().sum::<i32>())
        }).flatten().fold(0, |acc, i| {
            if i > 10 {
                acc + i - 10
            } else {
                acc
            }
        });

        up_if_max((total_impact * (1.0 - (over_hours as f32 / max_over_hours))) as i32, total_impact as i32)
    }

    fn due_hours_impact(&self, total_impact: f32) -> i32 {
        let total_due_hours = self.workers.iter().map(|w| w.start_due_hours).sum::<i32>();
        let total_expected = self.periods.iter().map(|w| w.expected).sum::<i32>();
        let best_ratio = total_expected  as f32 / total_due_hours as f32;

        let max_hours = self.periods.len() as i32;
        let num_workers = self.workers.len();

        let summed_diff = self.workers.iter().map(|w| {
            let best_hours = ((1.0 - best_ratio) * w.start_due_hours as f32) as i32;
            let diff = (w.due_hours - best_hours).abs() as f32;
            let worst = max_hours as f32 - w.start_due_hours as f32 + best_hours as f32;
            total_impact * (1.0 - (diff / worst))
        }).sum::<f32>();

        up_if_max((summed_diff / num_workers as f32) as i32, total_impact as i32)
    }

    pub fn evaluate(&self) -> i32 {
        let mut score: i32 = 0;

        score += self.filled_hours_impact(500000.0);
        score += self.work_period_len_impact(500000.0);
        score += self.off_hours_impact(10000.0);
        score += self.day_hours_impact(10000.0);
        score += self.due_hours_impact(4000.0);

        score
    }

    pub fn generate(&mut self, dna: &Dna) {
        self.reset();
        let ref mut workers = self.workers;
        let ref mut periods = self.periods;

        let mut current_day = periods[0].day;

        for ref mut period in periods {
            if period.day != current_day {
                workers.iter_mut().for_each(|w| w.end_day(7));
                current_day = period.day;
            }
            workers.iter_mut().for_each(|w| w.update_score(dna, period.start));
            workers.sort_by_key( |w| -w.score );
            let min_score = workers.last().unwrap().score.min(0).abs();
            let max_score = workers.first().unwrap().score.max(0).abs();

            let score_range = min_score.max(max_score).max(1);

            for ref mut worker in workers.iter_mut() {
                let diff = period.expected - period.filled;
                let place = diff as f64 + worker.score as f64 * (dna[4] as f64 / score_range as f64);
                let place_worker = cubic( place as i32, &dna[0..4]);
                if place_worker > 0 {
                    worker.add_working_time();
                    period.slots[worker.worker as usize] = Some(worker.worker);
                    period.filled += 1;
                } else {
                    worker.add_non_working_time();
                    period.slots[worker.worker as usize] = None;
                }
            }
        }
        workers.iter_mut().for_each(|w| w.end_day(7));
    }

    pub fn output_planning(&self) {
        let mut output = String::new();
        let mut current_week = self.periods[0].week;
        let mut current_day = self.periods[0].day;

        output += &format!("Week: {}\n", current_week);
        output += &format!("day: {:?}\n", current_day);
        for period in &self.periods {
            if current_week != period.week {
                output += &format!("Week: {}\n", period.week);
                current_week = period.week;
            }
            if current_day != period.day {
                output += &format!("day: {:?}\n", period.day);
                current_day = period.day;
            }
            let mut hour_status = format!("Ok: ");
            if period.expected < period.filled {
                hour_status = format!("OVER: {} on", period.filled)
            } else if period.expected > period.filled {
                hour_status = format!("MISSING: {} on", period.filled)
            }

            output += &format!("        {: <2?} => {}- {} {}\n", period.start, slots_output(&period.slots), hour_status, period.expected)
        }
        println!("{}", output);
    }
}

fn up_if_max(score: i32, max: i32) -> i32 {
    if score == max {
        score + 2000
    } else {
        score
    }
}

fn work_period_from_expectations(
    max_workers: usize,
    day_expectations: &Vec<(Day, Vec<Expectation>)>,
) -> Vec<WorkPeriod> {
    let mut work_periods = Vec::new();

    for (day, expectations) in day_expectations {
        for expectation in expectations {
            let start = expectation.period.start;
            for start in start..(start + expectation.period.len) {
                work_periods.push(WorkPeriod {
                    week: 1,
                    day: *day,
                    start,
                    expected: expectation.expected,
                    slots: vec![None; max_workers],
                    filled: 0
                })
            }
        }
    }
    work_periods
}

fn slots_output(slots: &Vec<Option<i32>>) -> String {
    let mut o = String::new();

    for e in slots {
        o += &match e {
            Some(i) => format!("[{}] ", i),
            None => "[ ] ".into()
        }
    }
    o
}