extern crate genetic_planning;

use std::env;
use std::process;

use genetic_planning::Config;

fn main() {
    let args: Vec<String> = env::args().collect();

    let config = Config::new(&args).unwrap_or_else(|err| {
        println!("Startup error: {}", err);
        process::exit(0);
    });

    if let Err(e) = genetic_planning::run(config) {
        println!("Application error: {}", e);
    }
}

