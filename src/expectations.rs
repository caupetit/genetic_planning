
use planning_generator::{ Expectation, Day };

pub fn get_expectations() -> Vec<(Day, Vec<Expectation>)> {
    vec!(
        (Day::Monday, vec!(
            Expectation::new(09, 02, 1), // Starting at 9h, for 2 hours, 1 employee is expected to work
            Expectation::new(11, 09, 2), // Starting at 9h, for 9 hours, 2 employees are expected to work
            Expectation::new(20, 03, 3), // ...
            Expectation::new(23, 03, 2),
        )),
        (Day::Tuesday, vec!(
            Expectation::new(09, 02, 1),
            Expectation::new(11, 15, 2),
        )),
        (Day::Wednesday, vec!(
            Expectation::new(09, 02, 1),
            Expectation::new(11, 01, 2),
            Expectation::new(12, 03, 3),
            Expectation::new(15, 11, 3),
        )),
        (Day::Thursday, vec!(
            Expectation::new(09, 02, 1),
            Expectation::new(11, 07, 2),
            Expectation::new(18, 06, 3),
            Expectation::new(24, 02, 2),
        )),
        (Day::Friday, vec!(
            Expectation::new(09, 02, 1),
            Expectation::new(11, 07, 2),
            Expectation::new(18, 06, 3),
            Expectation::new(24, 02, 2),
        )),
        (Day::Saturday, vec!(
            Expectation::new(09, 01, 1),
            Expectation::new(10, 01, 2),
            Expectation::new(11, 01, 3),
            Expectation::new(12, 11, 4),
            Expectation::new(23, 03, 2),
        )),
        (Day::Sunday, vec!(
            Expectation::new(09, 01, 1),
            Expectation::new(10, 01, 2),
            Expectation::new(11, 01, 3),
            Expectation::new(12, 06, 4),
            Expectation::new(18, 05, 3),
            Expectation::new(23, 03, 2),
        ))
    )
}