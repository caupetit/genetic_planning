extern crate rand;
extern crate time;

use rand::prelude::*;
use std::error::Error;
use time::PreciseTime;


mod planning_generator;
mod worker;
mod worker_analysis;
mod expectations;
mod genetics;

use planning_generator::PlanningGenerator;

use worker::Worker;
use genetics::genetic::Genetic;
use worker::WorkerPosition;
use expectations::get_expectations;

#[derive(Debug)]
pub struct Config {
    population: usize,
    generations: usize
}

impl Config {
    pub fn new(args: &[String]) -> Result<Config, &str> {
        if args.len() < 3 {
            return Err("Not enough arguments")
        }

        let population: usize = match args[1].parse() {
            Ok(n) => n,
            Err(_) => return Err("{Population must be an integer")
        };
        let generations: usize = match args[2].parse() {
            Ok(n) => n,
            Err(_) => return Err("Generation must be an integer")
        };

        Ok(Config { population, generations })
    }
}


pub fn run(config: Config) -> Result< (), Box<dyn Error> > {
    println!("Genetic Planning {:?}", config);

    let workers: Vec<Worker> = vec!(
        Worker::new(0, "Coo", 39, WorkerPosition::Peon),
        Worker::new(1, "Bee", 39, WorkerPosition::Peon),
        Worker::new(2, "App", 39, WorkerPosition::Peon),
        Worker::new(3, "Kii", 39, WorkerPosition::Peon),
        Worker::new(4, "Duu", 39, WorkerPosition::Peon),
        Worker::new(5, "Eaa", 25, WorkerPosition::Peon),
        Worker::new(6, "Fvv", 39, WorkerPosition::Manager),
        Worker::new(7, "Ghh", 39, WorkerPosition::Manager),
        Worker::new(8, "Ikk", 39, WorkerPosition::Manager),
    );

    let expectations = get_expectations();

    let due_hours: i32 = workers.iter().map(|w| w.due_hours).sum();
    let total_expected: i32 = expectations.iter().map(|e| {
        e.1.iter().map(|ex| ex.expected * ex.period.len).sum::<i32>()
    }).sum();
    println!("Starting for {} due hours and {} expected hours", due_hours, total_expected);

    let start = PreciseTime::now();
    let mut generator = PlanningGenerator::new(&workers, &expectations);
    {
        let mut genetics = Genetic::new(&mut generator, config.generations, config.population,thread_rng());
        genetics.process();
    }
    let end = PreciseTime::now();
    println!("Process took: {}", start.to(end));

//    generator.generate(&[2.882843, 44.799957, 41.85501, 100.64633, 90.72778, -61.86682, 50.29312, -72.372894, 61.03949, 29.018341, -59.998337, 41.547363, -25.939056, 27.496277, -63.223618, -71.77142, -22.014404, -63.18741, -14.190216, 45.67035, 0.7925415, 47.814697, -33.141357, -51.34825]);
    generator.output_planning();

    generator.workers.sort_by_key(|w| w.worker);
    for worker in generator.workers {
        println!("{:?}", worker);
    }

    Ok(())
}
