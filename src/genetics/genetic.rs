
use rand::prelude::*;
use std::fmt;

use planning_generator::PlanningGenerator;

pub(super) struct People {
    pub score: i32,
    pub diff: i32,
    dna: Dna
}

impl fmt::Debug for People {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut res = String::new();
        res += &format!("score: {:?} ", self.score);
        res += &format!("diff: {:?} ", self.diff);
        res += &format!("dna: {:?}", &self.dna[..]);
        write!(f, "{}", res)
    }
}

pub struct Genetic<'a> {
    generator: &'a mut PlanningGenerator,
    population: Vec<People>,
    rng: ThreadRng,
    generations: usize,
    population_nb: usize,
}

pub type Dna = [f32; DNA_LENGTH];

pub(super) const DNA_LENGTH: usize = 40;

const MIN_GENERATION: f32 = -100.0;
const MAX_GENERATION: f32 = 101.0;
const KEEP_PERCENTS: f32 = 0.35;


impl<'a> Genetic<'a> {
    pub fn new(generator: &mut PlanningGenerator, generations: usize, population: usize, rng: ThreadRng) -> Genetic {
        let mut instance = Genetic {
            generator,
            rng,
            population: Vec::new(),
            generations,
            population_nb: population,
        };
        instance.populate();
        instance
    }

    fn generate_and_evaluate(&mut self) {
        for i in 0..self.population_nb {
            let mut pop = &mut self.population[i];
            self.generator.generate(&pop.dna);
            let score = self.generator.evaluate();
            pop.score = score;
        }
    }

    fn output_generation_stats(&self, gen: usize) {
        if gen % 10 == 0 {
            println!("Gen {}, Best ones:", gen);
            for p in self.population.iter().take(15).collect::<Vec<_>>() {
                println!("{:?}", p);
            }
            output_score_repartition(self.population.iter().map(|x| x.score).collect());
        }
    }

    fn random_weighted_parent(&mut self, summed_score: i64) -> usize {
        let pick = self.rng.gen_range(0, (summed_score as f64 * 0.75) as i64);
        let mut current = 0;
        for (i, p) in self.population.iter().enumerate() {
            current += p.score.abs() as i64;
            if current > pick {
                return i;
            }
        }
        0
    }

    fn add_childrens(&mut self, new_dnas: &mut Vec<Dna>, summed_score: i64) {
        let parent1 = self.random_weighted_parent(summed_score);
        let parent2 = self.random_weighted_parent(summed_score);
        let parent1 = &self.population[parent1];
        let parent2 = &self.population[parent2];
        let child1 = integer_range(&parent1.dna, &parent2.dna, &mut self.rng);
        let child2 = integer_range(&parent2.dna, &parent1.dna, &mut self.rng);
        new_dnas.push(child1);
        new_dnas.push(child2);
    }

    fn update_difference(&mut self, gen: usize, new_dnas: &Vec<Dna>) {
        if gen > 0 && gen < 200 && new_dnas.len() > 0 {
            // diff score
            for p in &mut self.population {
                let diff = get_diff_with(&p.dna, &new_dnas);
                let mut gen_impact = gen as i32 / 10;
                if gen_impact == 0 {
                    gen_impact = 1
                }
                p.diff = (diff as i32 / gen_impact) * 10;
            }

            for p in self.population.iter_mut() {
                let score = p.score + p.diff * 10;
                p.diff = score;
            };
        }
        self.population.sort_by_key(|p| -p.diff);
    }

    fn generate_new_population(&mut self, gen: usize) {
        let mut loop_over = (self.population_nb as f32 * KEEP_PERCENTS) as usize;
        let mut new_dnas = Vec::with_capacity(self.population_nb);

        while loop_over < self.population_nb {
            self.update_difference(gen, &new_dnas);
            let summed_score: i64 = self.population.iter()
                .map(|p| p.score.abs() as i64)
                .sum::<i64>() + 1;
            self.add_childrens(&mut new_dnas, summed_score);
            loop_over += 2;
            self.population.sort_by_key(|p| -p.score);
        }

        let keep_pop_nb = (self.population_nb as f32 * KEEP_PERCENTS) as usize;
        for i in 0 .. keep_pop_nb {
            self.population[i].score = 0;
        }
        let mut dna_num: usize = 0;
        for i in keep_pop_nb .. self.population_nb {
            self.population[i].score = 0;
            self.population[i].dna = new_dnas[dna_num];
            dna_num += 1;
        }
    }

    pub fn process(&mut self) {
        for gen in 0 .. self.generations {
            self.generate_and_evaluate();
            self.population.sort_by_key(|p| -p.score);
            self.output_generation_stats(gen);
            self.generate_new_population(gen);
        }
        self.population.sort_by_key(|p| -p.score);
        println!("Best planning is: {:?}", &self.population[0].dna[..]);
        self.generator.generate(&self.population[0].dna);
    }

    fn populate(&mut self) {
        let mut population = Vec::new();

        for _ in 0 .. self.population_nb {
            let dna = self.random_dna();
            population.push(People { score: 0, diff: 0, dna });
        }
        self.population = population;
    }

    fn random_dna(& mut self) -> Dna {
        let mut dna = [0.0; DNA_LENGTH];

        for i in 0..DNA_LENGTH {
            dna[i] = self.rng.gen_range(MIN_GENERATION, MAX_GENERATION);
        }
        dna
    }
}

fn get_diff_with(current_dna: &Dna, new_dnas: &Vec<Dna>) -> f32 {
    let diff = |dna: &Dna| {
        let summed_squared_diff = dna.iter().zip(current_dna.iter())
            .map(|(a, b)| (a as &f32 - b as &f32).powi(2)).sum::<f32>();
        summed_squared_diff.sqrt()
    };

    new_dnas.iter().map(diff).sum::<f32>() / new_dnas.len() as f32
}

pub fn integer_range(dna1: &Dna, dna2: &Dna, rng: &mut ThreadRng) -> Dna {
    let mut child = [0.0; DNA_LENGTH];
    for i in 0 .. DNA_LENGTH {
        let (a, b) = (dna1[i], dna2[i]);
        let min = a.min(b) as f32;
        let max = a.max(b) as f32;
        let mut range = (max - min).abs() * 0.3;
        if range == 0.0 {
            range = (max.abs() * 0.08) + 0.01;
        }
        child[i] = rng.gen_range(min - range, max + range);
    }
    child
}


fn output_score_repartition(scores: Vec<i32>) {
    let min_score = *scores.iter().min().unwrap();
    let max_score = *scores.iter().max().unwrap();
    let score_range = max_score - min_score;

    let num_scores = scores.len();
    let unit = score_range / 100;
    let mut out = format!("Scores repartition: steps of {}\n", unit);
    let pop_ratio = 100.0 / num_scores as f32 / 10.0;
    let mut prev_limit = min_score;
    let mut repartition = Vec::new();
    for i in 0..100 {
        let limit = prev_limit + unit;
        let num = scores.iter().filter(|s| **s >= prev_limit && **s < limit).count();
        let num = num as f32 * pop_ratio;
        repartition.push((i, num));
        prev_limit = limit
    }
    for j in (0..10).rev() {
        out += &format!("{: >3?} - {: >3}% ",  j * 10,(j + 1) * 10);
        for i in 0..100 {
            if repartition[i].1 > j as f32 {
                out.push('|')
            } else {
                out.push(' ')
            }
        }
        out.push('\n');
    }
    out += &format!("           {: <20?}{: ^20?}{: ^20?}{: ^20?}{: >20?}",
                    min_score, min_score + score_range / 4,
                    min_score + score_range / 2,
                    max_score - score_range / 4, max_score);
    println!("{}", out);
}