use std::fmt;
use genetics::genetic::Dna;

#[derive(Debug)]
pub struct WorkerAnalysis {
    pub worker: i32,
    pub due_hours: i32,
    pub start_due_hours: i32,
    pub work_periods: Vec<Vec<i32>>,
    pub off_periods: Vec<i32>,
    current_work: bool,
    pub score: i32
}

pub fn cubic(x: i32, abcd: &[f32]) -> i32 {
    let (a, b, c, d) = (abcd[0], abcd[1], abcd[2], abcd[3]);
    let x = x as f32;

    (a * x.powi(3) + b * x.powi(2) + c * x + d) as i32
}

impl WorkerAnalysis {
    pub fn new(worker: i32, due_hours: i32) -> WorkerAnalysis {
        WorkerAnalysis {
            worker,
            due_hours,
            start_due_hours: due_hours,
            work_periods: vec!(vec!()),
            off_periods: vec!(12),
            current_work: false,
            score: 0
        }
    }

    pub fn add_working_time(&mut self) {
        self.due_hours -= 1;
        if !self.current_work {
            self.work_periods.last_mut().unwrap().push(0);
        }
        *self.work_periods.last_mut().unwrap().last_mut().unwrap() += 1;
        self.current_work = true;
    }

    pub fn add_non_working_time(&mut self) {
        if self.current_work {
            self.off_periods.push(0);
        }
        *self.off_periods.last_mut().unwrap() += 1;
        self.current_work = false;
    }

    pub fn end_day(&mut self, off_hours: i32) {
        if self.current_work {
            self.off_periods.push(0);
        }
        *self.off_periods.last_mut().unwrap() += off_hours;
        self.current_work = false;
        self.work_periods.push(vec!());
    }

    pub fn update_score(& mut self, dna: &Dna, start_period: i32) {
        let mut score = 1;

        // current period
        let period = *self.work_periods.last().unwrap().last().unwrap_or(&0);
        if period > 0 {
            let period_len = *self.work_periods.last().unwrap().last().unwrap_or(&0);
            score += cubic(period_len, &dna[8 .. 12]);
            score += cubic(start_period, &dna[12 .. 16]);
        }

        let off = *self.off_periods.last().unwrap_or(&0);
        if off > 0 && off < 24 {
            score += cubic( off % 24, &dna[16 .. 20]);
        }

        let hours_on_day: i32 = self.work_periods.last().unwrap().iter().sum();
        if hours_on_day > 0 {
            score += cubic( off % 24, &dna[20 .. 24]);
            score += cubic(hours_on_day, &dna[24..28]);
        }

        let mut due_hours_group = 0;
        due_hours_group += cubic(self.due_hours, &dna[28..32]);
        due_hours_group += cubic(off / 24, &dna[32 .. 36]);
        due_hours_group += cubic(off % 24, &dna[36 .. 40]);

        score += due_hours_group / 3;

        self.score = score;
    }

    pub fn reset(&mut self) {
        self.due_hours = self.start_due_hours;
        self.current_work = false;
        self.work_periods = vec!(vec!());
        self.off_periods = vec!(12);
        self.score = 0;
    }
}

impl fmt::Display for WorkerAnalysis {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "({}: due {} score: {} - work: {:?} off: {:?})", self.worker, self.due_hours, self.score, self.work_periods, self.off_periods)
    }
}