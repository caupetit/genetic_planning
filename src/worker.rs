#[derive(Debug, Copy, Clone)]
pub enum WorkerPosition {
    Peon,
    Manager
}

#[derive(Debug)]
pub struct Worker {
    pub id: i32,
    pub name: String,
    pub due_hours: i32,
    pub position: WorkerPosition
}

impl Worker {
    pub fn new(id: i32, name: &str, due_hours: i32, position: WorkerPosition) -> Worker {
        Worker { id, name: name.to_string(), due_hours, position }
    }
}
