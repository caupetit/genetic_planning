# GENETIC PLANNING

Planning generation using a genetic algorithm in RUST

# SETUP

install rust from https://www.rust-lang.org/fr/learn/get-started

run with bash from the root directory:
```bash
cargo build --release

time ./target/release/genetic_planning 400 80
```

* ./target/release/genetic_planning 400 80
    * 400 = population (400 plannings are generated from the DNA of each pop)
    * 80 = generation (number of time the DNA of the 400 pops are mixed up to find better ones)

# What did i just saw ?

## The main idea

The planning is generated hour by hour. A robot is asked to give a list of employees:

* monday 9AM to 10AM
    * LIST OF EMPLOYEES ==> (ROBOT) ==> Mick and Joe should work now
* monday 10AM to 11AM
    * LIST OF EMPLOYEES ==> (ROBOT) ==> Mick, Joe and John should work now

A genetic algorithm encodes a DNA of 40 floats with wich the robot is able to take a decision each ours


Every 10 generations, best dnas and the score repartition of the whole population are displayed

=> a best score of 1 031 800 + is considered a valid planning in the current state of the algorithm

# Rules

## le planning doit répondre aux regles suivantes

### Obligatoirement
* [] Planning sur 4 semaines (une seule semaine de faite)
* [x] 7 jours par semaine
* [x] tous les jours sont travailables du matin jusqu'as apres minuit (9h -> 25h)
* [x] a chaque heure un nombre d'employes MINIMUM doit etre present
- [x] 4h de travail / worker / jour minimum
- [x] 10h de travail / worker / jour maximum
- [x] les heures de travail sont consecutives avec une pause de 3 heures maximum
- [x] les heures de travail consecutives sont d'au moins 4h
- [x] 2 jours non travailles sur 7
- [x] 12h mininmum de repos entre deux journees travaillees
- [] un responsable doit etre present a la fermeture (nop)

### Au mieux

- [x] un minimum de journees en coupure
- [x] un minimum d'heures supp par employé
- [x] privilegier les heures en moins par rapport aux heures supp
- [~] un minimum d'heures non travailles par employe
- [] les jours non travailles sont le samedi et le dimanche (nop)
- [X] un grand minimum d'heure depassant les heures minumum voulues

## les employes

5 peons qui doivent travailler 39h

1 peon qui doit travailler 25h

3 responsables qui doivent travailler 39h

## Min workers / h / jour

Un jour commence a 9h le matin et termine a 2h du matin le lendemain (09h->26h)

* Lundi
    * 09h -> 11h = 1
    * 11h -> 20h = 2
    * 20h -> 23h = 3
    * 23h -> 26h = 2

* Mardi
    * 09h -> 11h = 1
    * 11h -> 26h = 2

* Mercredi
    * 09h -> 11h = 1
    * 11h -> 12h = 2
    * 12h -> 15h = 3 
    * 15h -> 26h = 2

* Jeudi
    * 09h -> 11h = 1
    * 11h -> 18h = 2
    * 18h -> 24h = 3
    * 24h -> 26h = 2

* Vendredi

    * = Jeudi
    
* Samedi
    * 09h -> 10h = 1
    * 10h -> 11h = 2
    * 11h -> 12h = 3
    * 12h -> 23h = 4
    * 23h -> 26h = 3
    
* Dimanche
    * 09h -> 10h = 1
    * 10h -> 11h = 2
    * 11h -> 12h = 3
    * 12h -> 18h = 4
    * 18h -> 23h = 3
    * 23h -> 26h = 2

Avec les 9 employés, cela fait 337 heures de travail pour 296 heures de travail possibles avec ce planning.

Une solution ou tous les employes font toutes leur heures n'est donc pas possible sans faire plus que les heures attendues. L'algo actuel n'essaie pas de gerer ca sur une seule semaine. Il faudrai le gerer sur un algo mensuel

Par contre l'algo essaie d'equilibrer les heures non travaillees entre les employés